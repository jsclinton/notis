package com.travel.notis.services;

import java.util.ArrayList;
import java.util.List;

import com.travel.notis.common.LatLong;
import com.travel.notis.common.TaxiInfo;

public class TaxiService 
{
	public List<TaxiInfo> GetTaxiInformation(LatLong fromLocation)
	{
		List<TaxiInfo> taxiInfoList = new ArrayList<TaxiInfo>();
		taxiInfoList.add(new TaxiInfo("Mockery Taxi Co.", "24 24 24 24", 5.50));
		taxiInfoList.add(new TaxiInfo("Real Taxis", "34 34 34 34", 6.00));
		taxiInfoList.add(new TaxiInfo("Taxxy", "43 43 43 43", 7.50));
		return taxiInfoList;
	}
}
