package com.travel.notis.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.travel.notis.common.AlternateFlightOption;
import com.travel.notis.common.FlightAlternative;
import com.travel.notis.common.dto.amadeus.Flight;
import com.travel.notis.common.dto.amadeus.FlightInfo;
import com.travel.notis.common.dto.amadeus.Result;
import com.travel.notis.configuration.APIKeys;
import com.travel.notis.configuration.DateTimeFormatters;

public class FlightService 
{
	private String extensiveSearchUrl = "https://api.sandbox.amadeus.com/v1.2/flights/low-fare-search?apikey=" + APIKeys.amadeus;	
	
	public FlightInfo GetFlightOptions(String originIATA, String destinationIATA, LocalDate departureDate)
	{
		String requestStr = extensiveSearchUrl;
		requestStr += "&origin=" + originIATA;
		requestStr += "&destination=" + destinationIATA;
		requestStr += "&departure_date=" + departureDate;		
		RestTemplate request = new RestTemplate();
		FlightInfo flightInfo = null;
		try
		{
			flightInfo = request.getForObject(requestStr, FlightInfo.class);
			//Restrict results to 1 currently
			flightInfo.setResults(flightInfo.getResults().subList(0,2));
		}
		catch (RestClientException ex)
		{
			System.out.println(ex.toString());
		}
		//Temp trim results to 3 or less
		//ArrayList<AlternateFlightOption> flightOptions = MapFlightInfo(flightInfo);
		//return new ArrayList<AlternateFlightOption>(flightOptions.subList(0,3));
		return flightInfo;
	}	
	
	//Redundant currently, could be useful at some point if we get time
	private ArrayList<AlternateFlightOption> MapFlightInfo(FlightInfo flightInfo)
	{
		List<AlternateFlightOption> flightOptions = new ArrayList<AlternateFlightOption>();
		for(Result result : flightInfo.getResults())
		{			
			if (result.getItineraries() == null)
			{
				continue;
			}			
			if (result.getItineraries().get(0).getOutbound() == null)
			{
				continue;
			}
			if (result.getItineraries().get(0).getOutbound().getFlights() == null)
			{
				continue;
			}
			if (result.getItineraries().get(0).getOutbound().getFlights().size() <= 0)
			{
				continue;
			}
			
			for (Flight flight : result.getItineraries().get(0).getOutbound().getFlights())
			{
				AlternateFlightOption flightOption = new AlternateFlightOption();
				flightOption.setDepartsAtDateTime(LocalDateTime.parse(flight.getDepartsAt(), DateTimeFormatters.Amadeus));
				flightOption.setArrivesAtDateTime(LocalDateTime.parse(flight.getArrivesAt(), DateTimeFormatters.Amadeus));
				if (flight.getOrigin() != null)
				{
					flightOption.setOriginAirport(flight.getOrigin().getAirport());
					flightOption.setOriginTerminal(flight.getOrigin().getTerminal());
				}
				if (flight.getDestination() != null)
				{
					flightOption.setDestinationAirport(flight.getDestination().getAirport());
					flightOption.setDestinationTerminal(flight.getDestination().getTerminal());
				}
				flightOption.setOperatingAirline(flight.getMarketingAirline());
				flightOption.setFlightNumber(flight.getFlightNumber());
				if (flight.getBookingInfo() != null)
				{
					flightOption.setTravelClass(flight.getBookingInfo().getTravelClass());
					flightOption.setSeatsRemaining(flight.getBookingInfo().getSeatsRemaining());				
				}
				if (result.getFare() != null)
				{
					flightOption.setTotalFare(result.getFare().getTotalPrice());
				}
				flightOptions.add(flightOption);				
			}			
		}
		return (ArrayList<AlternateFlightOption>) flightOptions;
	}
}
