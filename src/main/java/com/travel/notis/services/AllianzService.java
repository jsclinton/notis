package com.travel.notis.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.travel.notis.common.AllianzInsuranceResponse;
import com.travel.notis.common.AllianzTravelDetail;
import com.travel.notis.common.AllianzTravelServices;
import com.travel.notis.configuration.APIKeys;

public class AllianzService {

	public AllianzService() {
		
	}
	
	private String allainzBaseUrl = "https://aai-api.com/api/";
	
	public List<AllianzInsuranceResponse> GetAllianzServices() {

		String requestStr = allainzBaseUrl;
		requestStr += "products?apiKey=" + APIKeys.allianz;
		requestStr += "&category=TRAVEL";
	
		RestTemplate request = new RestTemplate();
		try
		{
			AllianzTravelServices[] allianzTravelServices = request.getForObject(requestStr, AllianzTravelServices[].class);
			
			AllianzTravelServices emergencyService = null;
			for (AllianzTravelServices travelService : allianzTravelServices) {
				if (travelService.getCode().equals("1008")) {
					emergencyService = travelService;
				}
			}
			
			List<AllianzInsuranceResponse> allianzInsuranceResponses = new ArrayList<AllianzInsuranceResponse>();
			
			allianzInsuranceResponses.add(new AllianzInsuranceResponse(
					emergencyService.getName(), 
					emergencyService.getDescription(), 
					emergencyService.getPrice().toString()));
			
			String requestMore = "https://aai-api.com/api/services?apiKey=test-apiKey-1&productCode=1008";
			AllianzTravelDetail[] allianzTravelDetail = request.getForObject(requestMore, AllianzTravelDetail[].class);
			
			for (AllianzTravelDetail travelDetail : allianzTravelDetail) {
				allianzInsuranceResponses.add(new AllianzInsuranceResponse(
						travelDetail.getName(), 
						travelDetail.getDescription(), 
						travelDetail.getPrice().toString()));
			}
			
			
			return allianzInsuranceResponses;

		}
		catch (RestClientException ex)
		{
			System.out.println(ex.toString(	));
		}
		catch (Exception e) {
			System.out.println(e.toString());
		}
		
//		try
//		{
//			URLConnection connection = new URL(requestStr).openConnection();
//			connection.setRequestProperty("Accept-Charset", "UTF-8");
//			InputStream inputStream = connection.getInputStream();
//			
//			StringWriter writer = new StringWriter();
//			IOUtils.copy(inputStream, writer, "UTF-8");
//			return writer.toString();
//		}
//		catch (Exception ex)
//		{
//			System.out.println(ex.toString());
//		}
	
		return null;
	}
}
