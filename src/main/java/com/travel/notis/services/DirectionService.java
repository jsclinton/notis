package com.travel.notis.services;

import org.springframework.web.client.RestTemplate;

import com.travel.notis.common.GoogleDirection;

public class DirectionService {

	private static String requestFormat = "https://maps.googleapis.com/maps/api/directions/json?origin=%s&destination=%s&mode=transit&key=AIzaSyCp0SQBCdarZmumWFOAnHkvZTzV7RCD8ZA";

	public DirectionService() {
	}

	public int GetDirections(String origin, String destination) {
		return DoRequest(String.format(requestFormat, origin, destination));
	}

	private int DoRequest(String requestStr) {

		RestTemplate request = new RestTemplate();
		GoogleDirection googleDirection = null;
		try {
			googleDirection = request.getForObject(requestStr,
					GoogleDirection.class);
			int duration = googleDirection.getRoutes().get(0).getLegs().get(0)
					.getDuration().getValue();
			
			return duration;
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		return 99999;
	}
}
