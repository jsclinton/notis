package com.travel.notis.datastore;

import java.util.ArrayList;
import java.util.List;

import com.travel.notis.common.CalendarEntry;
import com.travel.notis.common.CalendarEntryStage;
import com.travel.notis.common.Stage;
import com.travel.notis.common.TripStages;

public class DataStore {

	public static ArrayList<CalendarEntry> globalCalendarEntries = new ArrayList<CalendarEntry>();
	public static ArrayList<TripStages> globalTripStages = new ArrayList<TripStages>();
	
	public static void clearAllData()
	{
		globalCalendarEntries = new ArrayList<CalendarEntry>();
		globalTripStages = new ArrayList<TripStages>();
	}
	
	public static void addCalenderEntry(CalendarEntry calendarEntry)
	{
		globalCalendarEntries.add(calendarEntry);
	}
	
	public static void addCalendarEntryRange(List<CalendarEntry> calendarEntries)
	{
		for (CalendarEntry calEnt : calendarEntries)
		{
			addCalenderEntry(calEnt);
		}
	}
	
	public static void addTripStage(TripStages tripStage)
	{
		boolean isAdded = false;
		int foundYou = 0;
		for (int i = 0; i < globalTripStages.size(); i++)
		{
			if (tripStage.getCalendarEntryUUID().equals(globalTripStages.get(i).getCalendarEntryUUID()))
			{
				foundYou = i;
				isAdded = true;
			}
		}
		
		if (!isAdded) {
			globalTripStages.add(tripStage);
		} else {
			globalTripStages.remove(foundYou);
			globalTripStages.add(tripStage);
		}
	}
	
	public static CalendarEntryStage getCalendarEntryByStageId(String stageId)
	{
		CalendarEntryStage calEntryStage = new CalendarEntryStage();
		TripStages locTripStage = null;
		for (TripStages tripStage : globalTripStages)
		{
			for (Stage stage : tripStage.getStages())
			{
				if (stage.getStageId().toString().equals(stageId))
				{
					locTripStage = tripStage;
					calEntryStage.setStage(stage);
					break;
				}
			}
			if (locTripStage != null)
				break;
		}
		
		if (locTripStage == null)
		{
			System.out.println("Could not find the parent for stage with ID: " + stageId);
			return null;
		}
		
		for (CalendarEntry calendarEntry : globalCalendarEntries)
		{
			if (calendarEntry.getUuid().toString().equals(locTripStage.getCalendarEntryUUID()))
			{
				calEntryStage.setCalendarEntry(calendarEntry);
				return calEntryStage;
			}
		}
		System.out.println("Could not find the CalendarEntry for the Trip Stage: " + locTripStage.getCalendarEntryUUID());
		return null;
	}
}
