package com.travel.notis.common;

import com.travel.notis.common.dto.amadeus.FlightInfo;

public class FlightAlternative extends Alternative {

	public FlightInfo flightInfo;

	public FlightAlternative(String stageId) {
		super(stageId);
	}

	public FlightInfo getFlightInfo() {
		return flightInfo;
	}

	public void setFlightInfo(FlightInfo flightInfo) {
		this.flightInfo = flightInfo;
	}
}
