package com.travel.notis.common;

public class LatLong
{
	public double latitude;
	public double longitude;
	
	public LatLong(){}
	
	public LatLong(double latitude, double longitude)
	{
		this.latitude = latitude;
		this.longitude = longitude;			
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}				
}