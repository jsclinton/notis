package com.travel.notis.common;

public class TaxiInfo 
{
	String companyName;
	String telNumber;
	double farePrice;
	
	public TaxiInfo(String companyName, String telNumber, double farePrice)
	{
		this.companyName = companyName;
		this.telNumber = telNumber;
		this.farePrice = farePrice;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getTelNumber() {
		return telNumber;
	}
	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}
	public double getFarePrice() {
		return farePrice;
	}
	public void setFarePrice(double farePrice) {
		this.farePrice = farePrice;
	}
}
