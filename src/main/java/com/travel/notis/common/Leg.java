package com.travel.notis.common;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Leg {

	private ArrivalTime arrivalTime;
	private DepartureTime departureTime;
	private Duration duration;

	/**
	 * 
	 * @return The arrivalTime
	 */
	public ArrivalTime getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * 
	 * @param arrivalTime
	 *            The arrival_time
	 */
	public void setArrivalTime(ArrivalTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * 
	 * @return The departureTime
	 */
	public DepartureTime getDepartureTime() {
		return departureTime;
	}

	/**
	 * 
	 * @param departureTime
	 *            The departure_time
	 */
	public void setDepartureTime(DepartureTime departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * 
	 * @return The duration
	 */
	public Duration getDuration() {
		return duration;
	}

	/**
	 * 
	 * @param duration
	 *            The duration
	 */
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
}