package com.travel.notis.common;

public class TrainAlternative extends Alternative 
{
	public TrainInfo trainInfo;
	
	public TrainAlternative(String stageId) {
		super(stageId);
		// TODO Auto-generated constructor stub
	}

	public TrainInfo getTrainInfo() {
		return trainInfo;
	}

	public void setTrainInfo(TrainInfo trainInfo) {
		this.trainInfo = trainInfo;
	}

}
