package com.travel.notis.common;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.travel.notis.configuration.DateTimeFormatters;

public class AlternateFlightOption 
{
	@JsonIgnore
	private LocalDateTime departsAtDateTime;
	@JsonIgnore
	private LocalDateTime arrivesAtDateTime;
	private String departsAt;
	private String arrivesAt;
	private String originAirport;
	private String originTerminal;
	private String destinationAirport;
	private String destinationTerminal;
	private String operatingAirline;
	private String flightNumber;
	private String travelClass;
	private Integer seatsRemaining;
	private String totalFare;
		
	public AlternateFlightOption(){}
	
	public AlternateFlightOption(LocalDateTime departsAtDateTime, LocalDateTime arrivesAtDateTime,
			String originAirport, String originTerminal,
			String destinationAirport, String destinationTerminal,
			String operatingAirline, String flightNumber, String travelClass,
			Integer seatsRemaining, String totalFare) {
		super();
		this.departsAtDateTime = departsAtDateTime;
		this.arrivesAtDateTime = arrivesAtDateTime;
		this.originAirport = originAirport;
		this.originTerminal = originTerminal;
		this.destinationAirport = destinationAirport;
		this.destinationTerminal = destinationTerminal;
		this.operatingAirline = operatingAirline;
		this.flightNumber = flightNumber;
		this.travelClass = travelClass;
		this.seatsRemaining = seatsRemaining;
		this.totalFare = totalFare;
	}

	public LocalDateTime getDepartsAtDateTime() {
		return departsAtDateTime;
	}

	public void setDepartsAtDateTime(LocalDateTime departsAtDateTime) {
		this.departsAtDateTime = departsAtDateTime;
	}

	public LocalDateTime getArrivesAtDateTime() {
		return arrivesAtDateTime;
	}

	public void setArrivesAtDateTime(LocalDateTime arrivesAtDateTime) {
		this.arrivesAtDateTime = arrivesAtDateTime;
	}

	public String getDepartsAt() {
		return getDepartsAtDateTime().format(DateTimeFormatters.ISO8601);
	}

	public String getArrivesAt() {
		return getDepartsAtDateTime().format(DateTimeFormatters.ISO8601);
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public String getOriginTerminal() {
		return originTerminal;
	}

	public void setOriginTerminal(String originTerminal) {
		this.originTerminal = originTerminal;
	}

	public String getDestinationAirport() {
		return destinationAirport;
	}

	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public String getDestinationTerminal() {
		return destinationTerminal;
	}

	public void setDestinationTerminal(String destinationTerminal) {
		this.destinationTerminal = destinationTerminal;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getTravelClass() {
		return travelClass;
	}

	public void setTravelClass(String travelClass) {
		this.travelClass = travelClass;
	}

	public Integer getSeatsRemaining() {
		return seatsRemaining;
	}

	public void setSeatsRemaining(Integer seatsRemaining) {
		this.seatsRemaining = seatsRemaining;
	}

	public String getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	
}
