package com.travel.notis.common;

import java.util.ArrayList;
import java.util.List;

public class TripStages {

	public String calendarEntryUUID;
	public List<Stage> Stages;

	public TripStages(String calendarEntryUUID)
	{
		this.calendarEntryUUID = calendarEntryUUID;
		Stages = new ArrayList<Stage>();
	}
	
	public String getCalendarEntryUUID() {
		return calendarEntryUUID;
	}

	public void setCalendarEntryUUID(String calendarEntryUUID) {
		this.calendarEntryUUID = calendarEntryUUID;
	}

	public void AddStage(Stage stage)
	{
		Stages.add(stage);
	}
	
	public List<Stage> getStages() {
		return Stages;
	}

	public void setStages(List<Stage> stages) {
		Stages = stages;
	}

}
