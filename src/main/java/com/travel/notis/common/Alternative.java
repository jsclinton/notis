package com.travel.notis.common;

public class Alternative {

	public String stageId;

	public Alternative(String stageId) {
		this.stageId = stageId;
	}

	public String getStageId() {
		return stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}
}
