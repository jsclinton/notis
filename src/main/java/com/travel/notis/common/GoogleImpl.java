package com.travel.notis.common;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.travel.notis.configuration.DateTimeFormatters;
import com.travel.notis.services.GoogleService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

public class GoogleImpl {
	Credential credential;

	public List<CalendarEntry> GetCalendarEntries(String accessToken) {
		Events events = null;
		com.google.api.services.calendar.Calendar service;

		System.out.println("Trying to authenticate with code " + accessToken);

		try {
			this.credential = GoogleService.getCredentials(accessToken);
			service = GoogleService.getCalendarService(credential);

			// List the next 10 events from the primary calendar.
			DateTime now = new DateTime(System.currentTimeMillis());
			events = service.events().list("primary").setMaxResults(10)
					.setTimeMin(now).setOrderBy("startTime")
					.setSingleEvents(true).execute();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int entryCount = 1;
		List<CalendarEntry> calendarEntries = new ArrayList<CalendarEntry>();

		CalendarEntry mockEntryOutbound = new CalendarEntry("LCY", "FRA",
				LocalDateTime.of(2015, 06, 01, 14, 45), LocalDateTime.of(2015,
						06, 01, 17, 15), entryCount);
		calendarEntries.add(mockEntryOutbound);

		entryCount++;
		CalendarEntry mockEntryInbound = new CalendarEntry("FRA", "LCY",
				LocalDateTime.of(2015, 06, 02, 20, 10), LocalDateTime.of(2015,
						06, 01, 22, 40), entryCount);
		calendarEntries.add(mockEntryInbound);
		
		List<Event> items = events.getItems();
		if (items.size() == 0) {
			System.out.println("No upcoming events found.");
			return null;
		} else {
			for (Event event : items) {				
				DateTime start = event.getStart().getDateTime();
				if (start == null) {
					start = event.getStart().getDate();
				}

				if (event.getSummary().contains("Flight to")) {
					CalendarEntry entry = new CalendarEntry();

					String goingTo = event.getSummary().substring(10);
					entry.setToAirport(goingTo);
					entry.setFromAirport(event.getLocation());

					entry.setArrivalTime(LocalDateTime.parse(event.getEnd()
							.getDateTime().toString(),
							DateTimeFormatters.ISO8601));
					entry.setDepartureTime(LocalDateTime.parse(event.getStart()
							.getDateTime().toString(),
							DateTimeFormatters.ISO8601));
					entryCount++;
					entry.setTripNo(entryCount);
					
					calendarEntries.add(entry);
				}
			}
		}

		return calendarEntries;
	}

	public String GetCalendarEntries() {
		Events events = null;
		com.google.api.services.calendar.Calendar service;

		try {
			service = GoogleService.getCalendarService(credential);

			// List the next 10 events from the primary calendar.
			DateTime now = new DateTime(System.currentTimeMillis());
			events = service.events().list("primary").setMaxResults(10)
					.setTimeMin(now).setOrderBy("startTime")
					.setSingleEvents(true).execute();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String calendarEntries = "";

		List<Event> items = events.getItems();
		if (items.size() == 0) {
			System.out.println("No upcoming events found.");
			return "no items in calendar";
		} else {
			System.out.println("Upcoming events");
			for (Event event : items) {
				DateTime start = event.getStart().getDateTime();
				if (start == null) {
					start = event.getStart().getDate();
				}
				System.out.printf("%s (%s)\n", event.getSummary(), start);
				System.out.println("here " + event.getDescription());
				calendarEntries += "<p>And this: " + event.getSummary()
						+ "</p>";

				if (event.getSummary().contains("Flight to")) {
					calendarEntries += "The one above was a flight.";
					calendarEntries += "<p>getStatus: " + event.getStatus()
							+ "</p>";
				}
			}
		}

		return calendarEntries;
	}
}
