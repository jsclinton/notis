package com.travel.notis.common;

public class TripAlternativesParams 
{
	private String stageId;
	private LatLong currentLocation;
	
	public String getStageId() {
		return stageId;
	}
	public void setStageId(String stageId) {
		this.stageId = stageId;
	}
	public LatLong getCurrentLocation() {
		return currentLocation;
	}
	public void setCurrentLocation(LatLong currentLocation) {
		this.currentLocation = currentLocation;
	}
	
	
}
