package com.travel.notis.common;

import java.util.ArrayList;
import java.util.List;

import com.travel.notis.common.dto.amadeus.FlightInfo;

public class TripInformation {

	public LatLong origin;
	public LatLong destination;
	//public FlightInfo flightInfo;
	public String calendarEntryUUID;
	
	public TripInformation()
	{					
	}	
	
	public TripInformation(LatLong origin, LatLong destination, String calendarEntryUUID)
	{
		this.origin = origin;
		this.destination = destination;
		this.calendarEntryUUID = calendarEntryUUID;
	}
	
	public String getCalendarEntryUUID() {
		return calendarEntryUUID;
	}


	public void setCalendarEntryUUID(String calendarEntryUUID) {
		this.calendarEntryUUID = calendarEntryUUID;
	}


	public LatLong getOrigin() {
		return origin;
	}

	public void setOrigin(LatLong origin) {
		this.origin = origin;
	}

	public LatLong getDestination() {
		return destination;
	}

	public void setDestination(LatLong destination) {
		this.destination = destination;
	}	
}
