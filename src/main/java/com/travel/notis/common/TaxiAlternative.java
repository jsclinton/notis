package com.travel.notis.common;

import java.util.ArrayList;
import java.util.List;

public class TaxiAlternative extends Alternative {

	public List<TaxiInfo> taxiInfo;

	public TaxiAlternative(String stageId) {
		super(stageId);
		// TODO Auto-generated constructor stub
		taxiInfo = new ArrayList<TaxiInfo>();
	}

	public List<TaxiInfo> getTaxiInfo() {
		return taxiInfo;
	}

	public void setTaxiInfo(List<TaxiInfo> list) {
		this.taxiInfo = list;
	}
}
