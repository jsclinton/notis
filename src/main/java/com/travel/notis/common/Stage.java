package com.travel.notis.common;

import java.util.UUID;

public class Stage {

	private boolean statusOk;
	private int stageNo;
	private UUID stageId;
	private String statusReason;
	private String statusDescription;
	private String expectedLeaveTime;
	private String expectedArrivalTime;
	private String fromLocation;
	private String toLocation;

	public Stage() {
	}

	public Stage(boolean statusOk, int stageNo, String fromLocation,
			String toLocation, String expectedArrivalTime, String expectedLeaveTime) {
		this.statusOk = statusOk;
		this.stageNo = stageNo;
		this.expectedArrivalTime = expectedArrivalTime;
		this.expectedLeaveTime = expectedLeaveTime;
		this.fromLocation = fromLocation;
		this.toLocation = fromLocation;
		stageId = UUID.randomUUID();
	}

	public Stage(boolean statusOk, int stageNo, String fromLocation,
			String toLocation, String expectedArrivalTime, String expectedLeaveTime,
			String statusReason, String statusDescription) {
		this(statusOk, stageNo, fromLocation, toLocation, expectedArrivalTime, expectedLeaveTime);
		this.statusReason = statusReason;
		this.statusDescription = statusDescription;
	}

	public String getExpectedLeaveTime() {
		return expectedLeaveTime;
	}

	public void setExpectedLeaveTime(String expectedLeaveTime) {
		this.expectedLeaveTime = expectedLeaveTime;
	}

	public String getExpectedArrivalTime() {
		return expectedArrivalTime;
	}

	public void setExpectedArrivalTime(String expectedArrivalTime) {
		this.expectedArrivalTime = expectedArrivalTime;
	}

	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}

	public String getToLocation() {
		return toLocation;
	}

	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}

	public boolean isStatusOk() {
		return statusOk;
	}

	public void setStatusOk(boolean statusOk) {
		this.statusOk = statusOk;
	}

	public int getStageNo() {
		return stageNo;
	}

	public void setStageNo(int stageNo) {
		this.stageNo = stageNo;
	}

	public UUID getStageId() {
		return stageId;
	}

	public void setStageId(UUID stageId) {
		this.stageId = stageId;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
}
