package com.travel.notis.common;

import java.time.LocalDateTime;
import java.util.UUID;

public class CalendarEntry {

	private UUID uuid;

	private String fromAirport;

	private String toAirport;
	
	private LocalDateTime departureTime;
	
	private LocalDateTime arrivalTime;
	
	private int tripNo;
	
	public CalendarEntry() { 
		uuid = UUID.randomUUID();
	}
	
	public CalendarEntry(String fromAirport, String toAirport, LocalDateTime departureTime, LocalDateTime arrivalTime, int tripNo) {
		this.fromAirport = fromAirport;
		this.toAirport = toAirport;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.tripNo = tripNo;
		uuid = UUID.randomUUID();
	}
	
	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public boolean IsValid()
	{
		if (fromAirport == null || fromAirport == "")
			return false;
		if (toAirport == null || toAirport == "")
			return false;
		if (departureTime == null || arrivalTime == null)
			return false;
		return true;
	}
	
	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public String getToAirport() {
		return toAirport;
	}

	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public LocalDateTime getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(LocalDateTime departureTime) {
		this.departureTime = departureTime;
	}

	public LocalDateTime getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(LocalDateTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getTripNo() {
		return tripNo;
	}

	public void setTripNo(int tripNo) {
		this.tripNo = tripNo;
	}	
}
