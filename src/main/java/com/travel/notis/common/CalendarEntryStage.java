package com.travel.notis.common;

public class CalendarEntryStage {

	public CalendarEntry calendarEntry;
	public Stage stage;
	
	public CalendarEntry getCalendarEntry() {
		return calendarEntry;
	}
	public void setCalendarEntry(CalendarEntry calendarEntry) {
		this.calendarEntry = calendarEntry;
	}
	public Stage getStage() {
		return stage;
	}
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	
}
