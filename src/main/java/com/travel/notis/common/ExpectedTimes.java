package com.travel.notis.common;

public class ExpectedTimes {

	private String expectedArrivalTime;

	public String getExpectedArrivalTime() {
		return expectedArrivalTime;
	}

	public void setExpectedArrivalTime(String expectedArrivalTime) {
		this.expectedArrivalTime = expectedArrivalTime;
	}

	public String getExpectedLeaveTime() {
		return expectedLeaveTime;
	}

	public void setExpectedLeaveTime(String expectedLeaveTime) {
		this.expectedLeaveTime = expectedLeaveTime;
	}

	private String expectedLeaveTime;
}
