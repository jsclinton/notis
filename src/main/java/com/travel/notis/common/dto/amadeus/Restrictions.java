package com.travel.notis.common.dto.amadeus;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
	"refundable",
	"change_penalties"
})
public class Restrictions {

	@JsonProperty("refundable")
	private Boolean refundable;
	@JsonProperty("change_penalties")
	private Boolean changePenalties;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return
	 * The refundable
	 */
	@JsonProperty("refundable")
	public Boolean getRefundable() {
		return refundable;
	}

	/**
	 * 
	 * @param refundable
	 * The refundable
	 */
	@JsonProperty("refundable")
	public void setRefundable(Boolean refundable) {
		this.refundable = refundable;
	}

	/**
	 * 
	 * @return
	 * The changePenalties
	 */
	@JsonProperty("change_penalties")
	public Boolean getChangePenalties() {
		return changePenalties;
	}

	/**
	 * 
	 * @param changePenalties
	 * The change_penalties
	 */
	@JsonProperty("change_penalties")
	public void setChangePenalties(Boolean changePenalties) {
		this.changePenalties = changePenalties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
