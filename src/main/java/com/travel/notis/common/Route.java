package com.travel.notis.common;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Route {
	private List<Leg> legs = new ArrayList<Leg>();

	/**
	 * 
	 * @return The legs
	 */
	public List<Leg> getLegs() {
		return legs;
	}

	/**
	 * 
	 * @param legs
	 *            The legs
	 */
	public void setLegs(List<Leg> legs) {
		this.legs = legs;
	}

}