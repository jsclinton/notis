package com.travel.notis.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.travel.notis.common.Alternative;
import com.travel.notis.common.CalendarEntry;
import com.travel.notis.common.CalendarEntryStage;
import com.travel.notis.common.ExpectedTimes;
import com.travel.notis.common.FlightAlternative;
import com.travel.notis.common.GoogleDirection;
import com.travel.notis.common.Route;
import com.travel.notis.common.Stage;
import com.travel.notis.common.TaxiAlternative;
import com.travel.notis.common.TripAlternativesParams;
import com.travel.notis.common.TripStages;
import com.travel.notis.common.dto.amadeus.FlightInfo;
import com.travel.notis.datastore.DataStore;
import com.travel.notis.services.DirectionService;
import com.travel.notis.services.FlightService;
import com.travel.notis.services.TaxiService;

@Controller
public class TripController {

	FlightService flightService;
	TaxiService taxiService;

	public TripController() {
		flightService = new FlightService();
		taxiService = new TaxiService();
	}

	@RequestMapping("/trip")
	@ResponseBody
	public List<Stage> Trip(String calendarEntryUUID, String originLat,
			String originLong, String destinationLat, String destinationLong,
			HttpServletResponse response) {

		CalendarEntry thisEntry = null;

		for (CalendarEntry calendarEntry : DataStore.globalCalendarEntries) {
			if (calendarEntry.getUuid().toString().equals(calendarEntryUUID)) {
				thisEntry = calendarEntry;
				break;
			}
		}

		if (thisEntry == null)
		{
			System.out.println("Could not retrieve the calendar entry for CalendarEntryUUID: " 
					+ calendarEntryUUID);
			return new ArrayList<Stage>();
		}
		
		TripStages tripStages = null;		
		if (thisEntry.getTripNo() == 1) {
			ExpectedTimes stageOneTimes = getExpectedTimes(
					String.format("%s,%s", originLat, originLong),
					thisEntry.getFromAirport(), thisEntry.getDepartureTime(),
					true);

			tripStages = new TripStages(calendarEntryUUID);
			tripStages.AddStage(new Stage(false, 1, String.format("%s,%s",
					originLat, originLong), thisEntry.getFromAirport(),
					stageOneTimes.getExpectedArrivalTime(), stageOneTimes
							.getExpectedLeaveTime(), "Tube Strike", 
							"Industrial action is currently being undertaken causing a severe reduction in available services."));

			String leave = String.format("%d:%d", thisEntry.getDepartureTime()
					.getHour(), thisEntry.getDepartureTime().getMinute());
			String arrive = String.format("%d:%d", thisEntry.getArrivalTime()
					.getHour(), thisEntry.getArrivalTime().getMinute());

			tripStages.AddStage(new Stage(true, 2, thisEntry.getFromAirport(),
					thisEntry.getToAirport(), leave, arrive));

			ExpectedTimes stageThreeTimes = getExpectedTimes(
					thisEntry.getToAirport(),
					String.format("%s,%s", destinationLat, destinationLong),
					thisEntry.getArrivalTime(), false);

			tripStages.AddStage(new Stage(true, 3, thisEntry.getToAirport(),
					String.format("%s,%s", destinationLat, destinationLong),
					stageThreeTimes.getExpectedArrivalTime(), stageThreeTimes
							.getExpectedLeaveTime()));

			DataStore.addTripStage(tripStages);
		}
		else if (thisEntry.getTripNo() == 2)
		{		
			ExpectedTimes stageOneTimes = getExpectedTimes(
					String.format("%s,%s", originLat, originLong),
					thisEntry.getFromAirport(), thisEntry.getDepartureTime(),
					true);

			tripStages = new TripStages(calendarEntryUUID);
			tripStages.AddStage(new Stage(true, 1, String.format("%s,%s",
					originLat, originLong), thisEntry.getFromAirport(),
					stageOneTimes.getExpectedArrivalTime(), stageOneTimes
							.getExpectedLeaveTime()));

			String leave = String.format("%d:%d", thisEntry.getDepartureTime()
					.getHour(), thisEntry.getDepartureTime().getMinute());
			String arrive = String.format("%d:%d", thisEntry.getArrivalTime()
					.getHour(), thisEntry.getArrivalTime().getMinute());

			tripStages.AddStage(new Stage(false, 2, thisEntry.getFromAirport(),
					thisEntry.getToAirport(), leave, arrive,
					"Flight Cancellation",
					"Excessive fog has caused major disruption"));

			ExpectedTimes stageThreeTimes = getExpectedTimes(
					thisEntry.getToAirport(),
					String.format("%s,%s", destinationLat, destinationLong),
					thisEntry.getArrivalTime(), false);

			tripStages.AddStage(new Stage(true, 3, thisEntry.getToAirport(),
					String.format("%s,%s", destinationLat, destinationLong),
					stageThreeTimes.getExpectedArrivalTime(), stageThreeTimes
							.getExpectedLeaveTime()));

			DataStore.addTripStage(tripStages);
		}
		else
		{
			ExpectedTimes stageOneTimes = getExpectedTimes(
					String.format("%s,%s", originLat, originLong),
					thisEntry.getFromAirport(), thisEntry.getDepartureTime(),
					true);

			tripStages = new TripStages(calendarEntryUUID);
			tripStages.AddStage(new Stage(true, 1, String.format("%s,%s",
					originLat, originLong), thisEntry.getFromAirport(),
					stageOneTimes.getExpectedArrivalTime(), stageOneTimes
							.getExpectedLeaveTime()));

			String leave = String.format("%d:%d", thisEntry.getDepartureTime()
					.getHour(), thisEntry.getDepartureTime().getMinute());
			String arrive = String.format("%d:%d", thisEntry.getArrivalTime()
					.getHour(), thisEntry.getArrivalTime().getMinute());

			tripStages.AddStage(new Stage(true, 2, thisEntry.getFromAirport(),
					thisEntry.getToAirport(), leave, arrive));

			ExpectedTimes stageThreeTimes = getExpectedTimes(
					thisEntry.getToAirport(),
					String.format("%s,%s", destinationLat, destinationLong),
					thisEntry.getArrivalTime(), false);

			tripStages.AddStage(new Stage(true, 3, thisEntry.getToAirport(),
					String.format("%s,%s", destinationLat, destinationLong),
					stageThreeTimes.getExpectedArrivalTime(), stageThreeTimes
							.getExpectedLeaveTime()));		

			DataStore.addTripStage(tripStages);
		}
		
		System.out.println("We got origin lat of: " + originLat);
		System.out.println("We got origin long of: " + originLong);
		System.out.println("We got destination lat of: " + destinationLat);
		System.out.println("We got destination long of: " + destinationLong);
		
		response.setHeader("Access-Control-Allow-Origin", "*");		
		return tripStages.getStages();
	}

	private ExpectedTimes getExpectedTimes(String origin, String destination,
			LocalDateTime localDateTime, boolean removeTime) {

		DirectionService directionService = new DirectionService();
		int duration = directionService.GetDirections(origin, destination);

		int hourInSeconds = 3600;

		LocalDateTime leave = null;
		LocalDateTime arrival = null;

		if (removeTime) {
			leave = localDateTime
					.minusSeconds((long) (duration + hourInSeconds));
			arrival = localDateTime.minusSeconds((long) hourInSeconds);
		} else {
			leave = localDateTime
					.plusSeconds((long) (duration + hourInSeconds));
			arrival = localDateTime.plusSeconds((long) hourInSeconds);
		}

		ExpectedTimes expectedTimes = new ExpectedTimes();
		expectedTimes.setExpectedArrivalTime(String.format("%s:%s",
				makeMinLength(leave.getHour()), makeMinLength(leave.getMinute())));
		expectedTimes.setExpectedLeaveTime(String.format("%s:%s",
				makeMinLength(arrival.getHour()), makeMinLength(arrival.getMinute())));

		return expectedTimes;
	}
	
	private String makeMinLength(int time) {
		
		if (time < 10) {
			return "0" + time;
		} else {
			return "" + time;
		}
	}

	@RequestMapping("/trip/{id}")
	@ResponseBody
	public List<? extends Alternative> TripAlternatives(
			TripAlternativesParams tripAltParams, HttpServletResponse response) // @PathVariable("id")
																				// String
																				// stageId
	{
		List<Alternative> alternatives = new ArrayList<Alternative>();
		if (tripAltParams.getStageId() == null
				|| tripAltParams.getStageId() == "")
			return alternatives;

		CalendarEntryStage calendarEntryStage = DataStore
				.getCalendarEntryByStageId(tripAltParams.getStageId());

		if (calendarEntryStage.getCalendarEntry() == null
				|| !calendarEntryStage.getCalendarEntry().IsValid()
				|| calendarEntryStage.getStage() == null) {
			System.out
					.println("Could not retrieve alternative data for the stage with ID: "
							+ tripAltParams.getStageId());
			return new ArrayList<Alternative>();
		}

		if (calendarEntryStage.getStage().getStageNo() == 1
				|| calendarEntryStage.getStage().getStageNo() == 3) {
			// Taxi, train
			TaxiAlternative taxiAlt = new TaxiAlternative(
					tripAltParams.getStageId());
			taxiAlt.setTaxiInfo(taxiService.GetTaxiInformation(tripAltParams
					.getCurrentLocation()));
			alternatives.add(taxiAlt);
		}
		if (calendarEntryStage.getStage().getStageNo() == 2) {
			// airlines
			System.out.println("Getting alternative flights");
			FlightAlternative altFlight = new FlightAlternative(
					tripAltParams.getStageId());
			altFlight.setFlightInfo(flightService.GetFlightOptions(
					calendarEntryStage.getCalendarEntry().getFromAirport(),
					calendarEntryStage.getCalendarEntry().getToAirport(),
					calendarEntryStage.getCalendarEntry().getDepartureTime()
							.toLocalDate()));
			alternatives.add(altFlight);
			System.out.println("Retrieved alternative flights");
		}

		response.setHeader("Access-Control-Allow-Origin", "*");
		return alternatives;
	}
}
