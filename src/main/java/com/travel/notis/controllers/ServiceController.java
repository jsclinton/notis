package com.travel.notis.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.travel.notis.common.AllianzInsuranceResponse;
import com.travel.notis.services.AllianzService;

@Controller
public class ServiceController {

	public ServiceController() { }
	
	@RequestMapping("/service/allianz")
	@ResponseBody
	public List<AllianzInsuranceResponse> AllianzProductList(HttpServletResponse response) {
		AllianzService allianzService = new AllianzService();

		response.setHeader("Access-Control-Allow-Origin", "*");
		return allianzService.GetAllianzServices();
	}
}
