package com.travel.notis.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.travel.notis.common.CalendarEntry;
import com.travel.notis.common.GoogleImpl;
import com.travel.notis.datastore.DataStore;
import com.travel.notis.services.FlightService;

@Controller
public class AccountController {
	@Autowired
	GoogleImpl googleImpl;

	@Autowired
	FlightService flightService;
	
	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "What do you wanna do?";
	}
	
	@RequestMapping("/auth")
	@ResponseBody
	List<CalendarEntry> auth(String token, HttpServletResponse response) {
		DataStore.clearAllData();
		System.out.println("Inside auth");
		List<CalendarEntry> calendarEntries = googleImpl.GetCalendarEntries(token);
		System.out.println("Finished auth");
		
		//Save to amazing datastore
		DataStore.addCalendarEntryRange(calendarEntries);
		response.setHeader("Access-Control-Allow-Origin", "*");
		return calendarEntries;
	}
	
	@RequestMapping("/calendar")
	@ResponseBody
	String calendar() {
		System.out.println("Inside auth");
		return googleImpl.GetCalendarEntries();
	}
}
