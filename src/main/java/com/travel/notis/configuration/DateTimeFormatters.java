package com.travel.notis.configuration;

import java.time.format.DateTimeFormatter;

public class DateTimeFormatters 
{
	public static DateTimeFormatter Amadeus = 
			DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
	
	public static DateTimeFormatter ISO8601 = 
			DateTimeFormatter.ISO_DATE_TIME;
}
