package com.travel.notis;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.travel.notis.common.CalendarEntry;
import com.travel.notis.common.GoogleImpl;
import com.travel.notis.common.TripStages;
import com.travel.notis.services.FlightService;

@SpringBootApplication
public class NotisAmMain {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(NotisAmMain.class, args);
	}
	
	@Bean
	public GoogleImpl createGoogleImpl() {
		return new GoogleImpl();
	}
	
	@Bean
	public FlightService createFlightService() {
		return new FlightService();
	}
}